package com.mall.common;

/**
 * Created with IntelliJ IDEA.
 * Description: 全局常量
 * User: xxm
 * Date: 2018-02-02
 * Time: 23:14
 */
public class Const {

    public static final String CURRENT_USER = "currentUser";

    public static final String EMAIL = "email";
    public static final String USERNAME = "username";


    /**
     * 内部接口实现角色分组
     */
    public interface Role {
        int ROLE_CUSTOMER = 0; // 普通用户
        int ROLE_ADMIN = 1;  // 管理员
    }
}
