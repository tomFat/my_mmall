# 这是一个企业级电商项目

## 技术选型

* 核心框架：Spring Framework
* 视图框架：Spring MVC
* 持久层框架：MyBatis
* 数据库连接池：Alibaba Druid
* 日志管理：Logback
* 工具类：Apache Commons、Jackson、guava
* 文件服务器：Vsftpd
* 页面：Bootstrap、jQuery、自定义JS、Css
* 服务器：tomcat

## 模块设计

### 用户模块

* **功能介绍**
<img src="http://xxmmall.oss-cn-beijing.aliyuncs.com/introduce/module1-0.png" width="70%" height="50%" />

* **数据库表设计**
<img src="http://xxmmall.oss-cn-beijing.aliyuncs.com/introduce/module1-1.png" width="70%" height="50%" />

### 分类管理模块

* **功能介绍**
<img src="http://xxmmall.oss-cn-beijing.aliyuncs.com/introduce/module2-0.png" width="70%" height="50%" />

* **数据库表设计**
<img src="http://xxmmall.oss-cn-beijing.aliyuncs.com/introduce/module2-1.png" width="70%" height="50%" />

### 商品管理模块

* **功能介绍**
<img src="http://xxmmall.oss-cn-beijing.aliyuncs.com/introduce/module3-0.png" width="70%" height="50%" />
<img src="http://xxmmall.oss-cn-beijing.aliyuncs.com/introduce/module3-1.png" width="70%" height="50%" />

* **数据库表设计**
<img src="http://xxmmall.oss-cn-beijing.aliyuncs.com/introduce/module3-2.png" width="70%" height="50%" />